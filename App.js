import * as React from 'react';
import { WebView } from 'react-native-webview';
import NetInfo from "@react-native-community/netinfo";

export default class App extends React.Component {
  
  componentWillMount(){
    NetInfo.fetch().then(state => {
      console.log("Connection type", state.details);
      console.log("Is connected?", state.isConnected);
    });

  }
  
  render() {
    return <WebView source={{ uri: 'http://<flask server ip address>:3000/' }} style={{ marginTop: 40 }} />;
  }
}